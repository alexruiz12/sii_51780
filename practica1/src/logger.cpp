#include <sys/types.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#define TAM_BUFF 200
int main()

{
	mkfifo("/tmp/myfifo",0777);
	int fd=open("/tmp/myfifo",O_RDONLY);
	while(1){

		char buffer [TAM_BUFF];
		read(fd,buffer,sizeof(buffer));
 		printf("%s",buffer);

	}

	close(fd);
	unlink("/tmp/myfifo");
	return 0;

}
